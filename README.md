# Dex

A Pokédex written in ReactJS, making use of resources from Bulbapedia,
PokéAPI.co, and PokémonCries.
This is a learning project.

This project is pre-alpha. The above statements are simply plans and ideas,
and does not reflect what the final product is.

---

## Development Setup

### Getting started

```bash
git clone https://gitlab.com/Japorized/dex
cd dex
npm install
npm start
```

---

## Development Notes

### Directory structure

```
dex
├── README.md
├── create-react-app-README.md
├── public
└── src
    ├── components
    └── pages
```

#### Components

* Make each component a directory in `src/components`, with the main file as
  `index.tsx`, and style as `style.css`.
* Feel free to make subcomponents in the directory of the component

#### Pages

* For now, pages are no different from layouts
  (may separate these two notions later on).
* This is where components are placed to be rendered
  (except for the Navbar, for now).
