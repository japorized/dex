import React from 'react';
import {Link} from 'react-router-dom';
import './style.scss';

interface NavItem {
  name: string;
  path: string;
}

interface IProps {
}

interface IState {
  active?: boolean;
}

export default class Navbar extends React.Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);
    this.state = {active: false};
  }

  render() {
    const home: NavItem = {
      name: 'Home',
      path: '/',
    };
    const about: NavItem = {
      name: 'About',
      path: '/about',
    };
    const navItems: Array<NavItem> = [home, about];
    const renderedNavItems = navItems.map((item, index) => (
      <li key={index}>
        <Link to={item.path}>{item.name}</Link>
      </li>
    ));

    return (
      <nav className="Navbar">
        <ul className={this.state.active ? 'active' : ''}>{renderedNavItems}</ul>
        <button onClick={this.toggleNav} />
      </nav>
    );
  }

  toggleNav = (): void => {
    this.setState(lastState => ({
      active: !lastState.active
    }));
  }

}
